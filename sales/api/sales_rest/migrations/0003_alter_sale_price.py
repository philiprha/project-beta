# Generated by Django 4.0.3 on 2024-02-09 03:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0002_alter_salesperson_employee_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sale',
            name='price',
            field=models.TextField(max_length=15),
        ),
    ]
