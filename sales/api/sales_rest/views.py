from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
import requests

from .models import (
    AutomobileVO,
    Salesperson,
    Customer,
    Sale,
)

from .encoders import (
    AutomobileVOEncoder,
    SalespersonEncoder,
    CustomerEncoder,
    SaleEncoder,
)

# Create your views here.

@require_http_methods(["GET", "POST"])
def api_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            { "salespeople": salespeople },
            encoder=SalespersonEncoder,
        )
    else: #POST
        try:
            content = json.loads(request.body)
            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                { "message": "Could not create the salesperson" }
            )
            response.status_code=400
            return response


@require_http_methods(["GET", "DELETE"])
def api_salesperson(request, pk):
    if request.method == "GET":
        try:
            salesperson = Salesperson.objects.get(id=pk)
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False
            )
        except Salesperson.DoesNotExist:
            response = JsonResponse({ "message": "Salesperson does not exist" })
            response.status_code=404
            return response
    else: #DELETE
        try:
            salesperson = Salesperson.objects.get(id=pk)
            salesperson.delete()
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False
            )
        except Salesperson.DoesNotExist:
            return JsonResponse({ "message": "Salesperson does not exist" })
        

@require_http_methods(["GET", "POST"])
def api_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            { "customers": customers },
            encoder=CustomerEncoder,
        )
    else: #POST
        print(request.body)
        try:
            content = json.loads(request.body)
            customers = Customer.objects.create(**content)
            return JsonResponse(
                customers,
                encoder=CustomerEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                { "message": "Could not create the customer" }
            )
            response.status_code=400
            return response


@require_http_methods(["GET", "DELETE"])
def api_customer(request, pk):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=pk)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False
            )
        except Customer.DoesNotExist:
            response = JsonResponse({ "message": "Customer does not exist" })
            response.status_code=404
            return response
    else: #DELETE
        try:
            customer = Customer.objects.get(id=pk)
            customer.delete()
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False
            )
        except Customer.DoesNotExist:
            return JsonResponse({ "message": "Customer does not exist" })


@require_http_methods(["GET", "POST"])
def api_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            { "sales": sales },
            encoder=SaleEncoder
        )
    else: #POST
        try:
            content = json.loads(request.body)
            # Salesperson
            salesperson_id = content["salesperson"]
            salesperson = Salesperson.objects.get(pk=salesperson_id)
            content["salesperson"] = salesperson
            # Customer
            customer_id = content["customer"]
            customer = Customer.objects.get(pk=customer_id)
            content["customer"] = customer
            # Automobile
            automobile_vo_vin = content["automobile"]
            automobile_vo = AutomobileVO.objects.get(vin=automobile_vo_vin)
            automobile_vo.sold = True
            automobile_vo.save()
            content["automobile"] = automobile_vo
            # Sale Creation
            sale = Sale.objects.create(**content)
            return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False
            )
        except:
            response = JsonResponse(
                { "message": "Could not create the sale record"}
            )
            response.status_code=400
            return response


@require_http_methods(["GET", "DELETE"])
def api_sale(request, pk):
    if request.method == "GET":
        try:
            sale = Sale.objects.get(id=pk)
            return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False
            )
        except Sale.DoesNotExist:
            response = JsonResponse({ "message": "Sale does not exist" })
            response.status_code=404
            return response
    else: #DELETE
        try:
            sale = Sale.objects.get(id=pk)
            sale.delete()
            return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False
            )
        except Sale.DoesNotExist:
            return JsonResponse({ "message": "Sale does not exist" })