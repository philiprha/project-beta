from django.urls import path, include

from .views import (
    api_salespeople,
    api_salesperson,
    api_customers,
    api_customer,
    api_sales,
    api_sale,
)

urlpatterns = [
    path(
        "salespeople/", 
         api_salespeople, #GET, POST
         name="api_salespeople"
    ),
    path(
        "salespeople/<int:pk>/",
         api_salesperson, #GET, DELETE
         name="api_salesperson",
    ),
    path(
        "customers/", 
        api_customers, #GET, POST
        name="api_customers",
    ),
    path(
        "customers/<int:pk>/",
        api_customer, #GET, DELETE
        name="api_customer",
    ),
    path(
        "sales/",
        api_sales, #GET, POST
        name="api_sales",
    ),
    path(
        "sales/<int:pk>/",
        api_sale, #GET, DELETE
        name="api_sale",
    ),
]