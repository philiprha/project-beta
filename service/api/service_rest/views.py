from django.shortcuts import render
from common.json import ModelEncoder
from django.http import JsonResponse
from .models import Technician, AutomobileVO, Appointment
import json
from django.views.decorators.http import require_http_methods

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold"
    ]

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id"
    ]

class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "vip",
        "date",
        "datetime",
        "reason",
        "status",
        "vin",
        "id",
        "technician",
        "customer_name"
    ]
    encoders = {
        "technician": TechnicianEncoder(),
                }

@require_http_methods(["GET", "POST"])
def api_list_technician(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
        {"technicians": technicians},
        encoder=TechnicianEncoder,
        )
    
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False
        )

@require_http_methods(["GET", "POST"])
def api_list_appointment(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            technician = Technician.objects.get(employee_id=content["technician"])
            content["technician"] = technician
        
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Technician ID"},
                status=400
            )

        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False
        )
        
# Create your views here.

@require_http_methods(["PUT"])
def api_update_appointment(request, pk):
    content = json.loads(request.body)
    Appointment.objects.filter(id=pk).update(**content)
    appointment = Appointment.objects.get(id=pk)
    return JsonResponse(
        appointment,
        encoder=AppointmentEncoder,
        safe=False,
    )