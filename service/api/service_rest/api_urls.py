from django.urls import path
from .views import api_list_technician, api_list_appointment, api_update_appointment

urlpatterns = [
    path("technicians/", api_list_technician, name="api_list_technician"),
    path("appointments/", api_list_appointment, name="api_list_appointment"),
    path("appointments/<int:pk>/", api_update_appointment, name="api_update_appointment"),
]