from django.db import models
from django.urls import reverse

# Create your models here.
class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

class Technician(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.SmallIntegerField(unique=True)

class Appointment(models.Model):
    vip = models.BooleanField(default=False)
    date = models.CharField(max_length=200)       
    datetime = models.CharField(max_length=200)
    reason = models.CharField(max_length=200)
    status = models.CharField(max_length=200, default="In Process")
    vin = models.CharField(max_length=200)
    customer_name = models.CharField(max_length=200, default="foo")

    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE,
    )
    def get_api_url(self):
        return reverse("api_update_appointment", kwargs={"pk": self.pk})