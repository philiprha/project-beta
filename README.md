# CarCar

* Philip Rha - Service API, Inventory
* Rydel Hutchins - Sales API, Inventory

######## How to Run this App ######## 
# 1. Fork this repository (repo)
#
# 2. Clone the forked repo onto your local computer:
# ====================================================
# git clone https://gitlab.com/philiprha/project-beta
# ====================================================
#
# 3. Build and run the project using Docker with these commands:
# ===============================
# docker volume create beta-data
# docker-compose build
# docker-compose up
# ===============================
#
# 4. Ensure your Docker Containers are running
# 
# 5. View the React front-end of the project in the browser at 
# =======================
# http://localhost:3000/
# =======================
#
######## Diagram ########
 - Put diagram here

######## API Documentation ########
 Our APIs operate on three back-end Django Projects each hosting 1 Django App
 Inventory microservices is accessed by Sales and Service microservices via their respective Pollers to create a
 perfect ecosystem for our Vehicle Management Application
 
 

######## URLs and Ports ########
Inventory API - http://localhost:8100/api/
Service API   - http://localhost:8080/api/
Sales API     - http://localhost:8090/api/

######## Inventory API ########
 - All vehicle records are kept here.  It contains data such as Vehicle Make, Vehicle Model, and Automobiles in Stock.
 - This data is stored using 3 models:
================
Manufacturer
Model
Automobile
================

######## Service API ########
 - Service microservice maintains, updates, and creates all needed data produced by the vehicular repair shop in your dealership. 
   It pulls the automobile vin information from Inventory microservice using a poller and determine the VIP status of vehicles in the appointments list.
   The microservice is also able to create technician information and attach technican to created appointments.

 - Through the Service microservice, it's easy to create, update, and cancel appointments with a click of a button!
 - Interact with Service API backend by Insomnia or a browser using following URL.
=====================================================================================
Action | Method | URL

1. List Current Appointments | GET | http://localhost:8080/api/appointments/
2. Create Appointmentss | POST | http://localhost:8080/api/appointments/
3. List Technicians | GET | http://localhost:8080/api/technicians/
4. Create Technician Entry | POST | http://localhost:8080/api/technicians/
=====================================================================================
1. GET from `/api/appointments/` will return an object encoded in JSON.
   The Object will be named 'appointments' and will contain a list of objects:
```
{
	"appointments": [
		{
			"href": "/api/appointments/5/",
			"vip": true,
			"date": "2024-03-05",
			"datetime": "21:55",
			"reason": "Transmission",
			"status": "In Process",
			"vin": "12345",
			"id": 5,
			"technician": {
				"first_name": "Philip",
				"last_name": "Rha",
				"employee_id": 17,
				"id": 1
			},
			"customer_name": "Jon Travolta"
		}]}
```
2. POST to `/api/appointments/` will be required to be encoded in JSON:
```
{	
	
	"date": "2024-03-05",
	"datetime": "21:55",
	"reason": "Transmission",
	"vin": "12345",
  "technician": "1",
	"customer_name": "Jon Travolta"
}
```
3. GET from `/api/technicians/` will return an object encoded in JSON.
   The Object will be named 'technicians' and will contain a list of objects.

```
{
	"technicians": [
		{
			"first_name": "Philip",
			"last_name": "Rha",
			"employee_id": 17,
			"id": 1
		}
	]
}
```
4. POST to `/api/technicians/` will be required to be encoded in JSON.
   
```
{
			"first_name": "Philip",
			"last_name": "Rha",
			"employee_id": 17
		}
```
######## Sales API ########
 - All Customer, Salespeople, and Sales records are kept here.
 - This data is stored using 4 Models:
================
AutomobileVO
Customer
Salesperson
Sales
================

- The Sale model interacts via Foreign Keys with the other 3 models.
- A poller is utilized to collect Automobile data from the inventory service into our Automobile Value Object (modeled as AutomobileVO).
- To interact with the Sales API you must make a request from the following URLs as instructed below:
=====================================================================================
Action | Method | URL

1. List salespeople     | GET  | http://localhost:8090/api/salespeople/
2. Create a salesperson | POST | http://localhost:8090/api/salespeople/
3. List customers       | GET  | http://localhost:8090/api/customers/
4. Create a customer    | POST | http://localhost:8090/api/customers/
5. List sales           | GET  | http://localhost:8090/api/sales/
6. Create a sale        | POST | http://localhost:8090/api/sales/
=====================================================================================

1. GET from `/api/salespeople/` will return an object encoded in JSON.
   The Object will be named 'salespeople' and will contain a list of objects:
```
{
  "salespeople": [
    {
      "href": "/api/salespeople/1/",
      "id": 1,
      "first_name": "Patrick",
      "last_name": "Star",
      "employee_id": "pstar"
    },
  ]
}
```
2. POST to `/api/salespeople/` will be required to be encoded in JSON:
```
{
  "first_name": "Patrick",
  "last_name": "Star", 
  "employee_id": "pstar"
}
```
3. GET from `/api/customers/` will return an object encoded in JSON.
   The Object will be named 'customers' and will contain a list of objects:
```
{
  "customers": [
    {
      "href": "/api/customers/1/",
      "id": 1,
      "first_name": "Spongebob",
      "last_name": "Squarepants",
      "phone_number": "111-111-1111",
      "address": "Bikini Bottom Road"
    },
  ]
}
```
4. POST to `/api/customers/` will be required to be encoded in JSON:
```
{
  "first_name": "Spongebob",
  "last_name": "Squarepants",
  "phone_number": "111-111-1111",
  "address": "Bikini Bottom Road"
}
```
5. GET from `/api/sales/` will return an object encoded in JSON.
   The Object will be named 'sales' and will contain a list of objects.
   Each "Sale" object will contain 3 objects within:
    - "automobile"
    - "salesperson"
    - "customer"
```
{
  "sales": [
    {
      "href": "/api/sales/1/",
      "id": 1,
      "price": "50000",
      "automobile": {
        "id": 1,
        "vin": "C00LV1N",
        "sold": true
      },
      "salesperson": {
        "href": "/api/salespeople/1/",
        "id": 1,
        "first_name": "Patrick",
        "last_name": "Star",
        "employee_id": "pstar"
      },
      "customer": {
        "href": "/api/customers/1/",
        "id": 1,
        "first_name": "Spongebob",
        "last_name": "Squarepants",
        "phone_number": "111-111-1111",
        "address": "Bikini Bottom Road"
      }
    },
  ]
}
```
6. POST to `/api/customers/` will be required to be encoded in JSON.
   It will require EXISTING Automobile, Salesperson, and Customer objects.
   This API interacts with AutomobileVO model.  The AutomobileVO model has two variables:
    - vin  : string
    - sold : boolean
   In order to create a record of sale the "sold" boolean must be changed to `true` it is stored as a bool, not a string.
   Upon changing this to true, a "PUT" request must also be made to the Automobile API so the specified automobile may be updated to "sold"
   
```
{
  "price": "50000",
  "automobile": {
    "id": 1,
    "vin": "C00LV1N",
    "sold": true
  },
  "salesperson": {
    "href": "/api/salespeople/1/",
    "id": 1,
    "first_name": "Patrick",
    "last_name": "Star",
    "employee_id": "pstar"
  },
  "customer": {
    "href": "/api/customers/1/",
    "id": 1,
    "first_name": "Spongebob",
    "last_name": "Squarepants",
    "phone_number": "111-111-1111",
    "address": "Bikini Bottom Road"
  }
}
```

















