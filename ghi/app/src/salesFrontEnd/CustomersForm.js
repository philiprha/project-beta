import React, {useState} from 'react';

function CustomersForm (props) {
    const [formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        phone_number: '',
        address: '',
    });

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    }


    const handleSubmit = async (event) => {
        event.preventDefault();
    
        const CustomersUrl = "http://localhost:8090/api/customers/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch(CustomersUrl, fetchConfig);
    }
    return(
        <div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <div className="text-center">
                <h1>Add New Customer</h1>
            </div>
            <form onSubmit={handleSubmit} id="create-customer-form">
                <div className="form-floating mb-3">
                    <input value={formData.first_name} onChange={handleFormChange} placeholder="first_name" required type="text" name="first_name" id="first_name" className="form-control" />
                    <label htmlFor="name">First Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={formData.last_name} onChange={handleFormChange} placeholder="last_name" required type="text" name="last_name" id="last_name" className="form-control" />
                    <label htmlFor="name">Last Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={formData.phone_number} onChange={handleFormChange} placeholder="phone_number" required type="text" name="phone_number" id="phone_number" className="form-control" />
                    <label htmlFor="name">Phone Number</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={formData.address} onChange={handleFormChange} placeholder="address" required type="text" name="address" id="address" className="form-control" />
                    <label htmlFor="name">Address</label>
                </div>
                <button className="btn btn-primary w-100">Create</button>
            </form>
        </div>
        </div>
        </div>    
    )
}

export default CustomersForm;