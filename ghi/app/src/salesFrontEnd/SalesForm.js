import React, { useEffect, useState } from 'react';

function SalesForm (prop) {
    // Get Automobile
    const [automobiles, setAutomobiles] = useState([]);
    const getAutomobiles = async () => {
        const r = await fetch('http://localhost:8100/api/automobiles/');
        if (r.ok) {
            const data = await r.json();
            setAutomobiles(data.autos);
        }
    };
    // Get Salespeople
    const [salespeople, setSalespeople] = useState([]);
    const getSalespeople = async () => {
        const r = await fetch('http://localhost:8090/api/salespeople/');
        if (r.ok) {
            const data = await r.json();
            setSalespeople(data.salespeople);
        }
    };
    // Get Customers
    const [customers, setCustomers] = useState([]);
    const getCustomers = async () => {
        const r = await fetch('http://localhost:8090/api/customers/');
        if (r.ok) {
            const data = await r.json();
            setCustomers(data.customers);
        }
    };

    useEffect(()=> {
        getAutomobiles();
        getSalespeople();
        getCustomers();
    }, []); 
    // ===============

    // Sales
    const [formData, setFormData] = useState({
        price: '',
        automobile: '',
        salesperson: '',
        customer: '',
    });

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value,
        });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        const SalesUrl = "http://localhost:8090/api/sales/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'applications/json'
            },
        };

        const AutoUrl = `http://localhost:8100/api/automobiles/${formData.automobile}/`
        const autoConfig = {
            method: "put",
            body: JSON.stringify({sold: true}),
            headers: {
                'Content-Type': 'applications/json'
            },
        };

        const response = await fetch(SalesUrl, fetchConfig);
        const autoPut = await fetch(AutoUrl, autoConfig);

        if (response.ok) {
            setFormData({
                price: '',
                automobile: '',
                salesperson: '',
                customer: '',
            })
        }
    };
    
    return(
        <div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <div className="text-center">
                <h1>Record New Sale</h1>
            </div>
            <form onSubmit={handleSubmit} id="create-sales-form">
                {/* Automobile VIN */}
                <div className="mb-3">
                    <select value={formData.automobile} onChange={handleFormChange} required name="automobile" id="automobile" className="form-select py-3">
                        <option value="">Automobile VIN</option>
                        { automobiles.map(automobile => {
                            if (!automobile.sold) {
                                return (
                                    <option key={automobile.id} value={automobile.vin}>
                                        {automobile.vin}
                                    </option>
                                    );
                                    }})
                        }
                    </select>
                </div>
                {/* Salesperson */}
                <div className="mb-3">
                    <select value={formData.salesperson} onChange={handleFormChange} required name="salesperson" id="salesperson" className="form-select py-3">
                        <option value="">Salesperson</option>
                        { salespeople.map(salesperson => {
                            return (
                                <option key={salesperson.id} value={salesperson.id}>
                                    {salesperson.first_name + " " + salesperson.last_name}
                                </option>
                                );
                                })
                        }
                    </select>
                </div>
                {/* Customer */}
                <div className="mb-3">
                    <select value={formData.customer} onChange={handleFormChange} required name="customer" id="customer" className="form-select py-3">
                        <option value="">Customer</option>
                        { customers.map(customer => {
                            return (
                                <option key={customer.id} value={customer.id}>
                                    {customer.first_name + " " + customer.last_name}
                                </option>
                                );
                                })
                        }
                    </select>
                </div>
                {/* Price */}
                <div className="form-floating mb-3">
                    <input value={formData.price} onChange={handleFormChange} placeholder="price" required type="number" name="price" id="price" className="form-control" />
                    <label htmlFor="name">Price</label>
                </div>
            <button className="btn btn-primary w-100">Submit</button>
            </form>
        </div>
        </div>
        </div>

    )
}

export default SalesForm;