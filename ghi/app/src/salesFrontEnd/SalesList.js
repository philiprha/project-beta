import React, { useEffect, useState } from 'react';

function SalesList(prop) {
    // Get Sales
    const [sales, setSales] = useState([]);
    const getSales = async () => {
        const response = await fetch('http://localhost:8090/api/sales/');
        if (response.ok) {
            const data = await response.json();
            setSales(data.sales);
        }
    };

    // Get Salespeople
    const [salespeople, setSalespeople] = useState([]);
    const getSalespeople = async () => {
        const r = await fetch('http://localhost:8090/api/salespeople/');
        if (r.ok) {
            const data = await r.json();
            setSalespeople(data.salespeople);
        }
    };

    useEffect(()=>{
        getSales();
        getSalespeople();
    }, []);

    const [formData, setFormData] = useState({
        employee_id: '',
    })

    const handleSalespersonChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        });
        
    };

    function renderFilter() {
        console.log("Did we make it?")
        if (formData.employee_id === '') {
            return (
                sales.map(sale => {
                    return(
                        <tr key={sale.id}>
                            <td>{sale.salesperson.employee_id}</td>
                            <td>{sale.salesperson.first_name + " " + sale.salesperson.last_name}</td>
                            <td>{sale.customer.first_name + " " + sale.customer.last_name}</td>
                            <td>{sale.automobile.vin}</td>
                            <td>{"$" + parseFloat(sale.price).toLocaleString('en-US')}</td>
                        </tr>
                    )
                })
            )
        } else {
            return (
                sales.filter(sale => sale.salesperson.employee_id === formData.employee_id).map(sale => {
                    return(
                        <tr key={sale.id}>
                            <td>{sale.salesperson.employee_id}</td>
                            <td>{sale.salesperson.first_name + " " + sale.salesperson.last_name}</td>
                            <td>{sale.customer.first_name + " " + sale.customer.last_name}</td>
                            <td>{sale.automobile.vin}</td>
                            <td>{"$" + parseFloat(sale.price).toLocaleString('en-US')}</td>
                        </tr>
                    )
                })
            )
        }
    }

    return (
        <div>
            <div>
                <h1>Sales History</h1>
                <select value={formData.employee_id} onChange={handleSalespersonChange} required name="employee_id" id="employee_id" className="form-select py-3 w-100">
                    <option value="">Filter by Salesperson</option>
                    { salespeople.map(salesperson => {
                        return (
                            <option key={salesperson.id} value={salesperson.employee_id}>
                                {salesperson.first_name + " " + salesperson.last_name}
                            </option>
                            );
                        })
                    }
                </select>
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Salesperson Employee ID</th>
                        <th>Salesperson Name</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {renderFilter()}
                </tbody>
            </table>
        </div>
    )
}

export default SalesList;