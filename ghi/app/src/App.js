import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';

// Inventory Front-End
import ManufacturersList from './inventoryFrontEnd/ManufacturersList';
import ManufacturersForm from './inventoryFrontEnd/ManufacturersForm';
import VehicleModelsList from './inventoryFrontEnd/VehicleModelsList';
import VehicleModelsForm from './inventoryFrontEnd/VehicleModelsForm';
import AutomobilesList from './inventoryFrontEnd/AutomobilesList';
import AutomobilesForm from './inventoryFrontEnd/AutomobilesForm';

// Service Front-End
import TechniciansForm from './serviceFrontEnd/TechniciansForm';
import TechniciansList from './serviceFrontEnd/TechniciansList';
import AppointmentsForm from './serviceFrontEnd/AppointmentsForm';
import AppointmentsList from './serviceFrontEnd/AppointmentsList';
import AppointmentsListVin from './serviceFrontEnd/AppointmentsListVin';
import AppointmentsHistory from './serviceFrontEnd/AppointmentsHistory';


// Sales Front-End
import CustomersForm from './salesFrontEnd/CustomersForm';
import CustomersList from './salesFrontEnd/CustomersList';
import SalesForm from './salesFrontEnd/SalesForm';
import SalesList from './salesFrontEnd/SalesList';
import SalespeopleForm from './salesFrontEnd/SalespeopleForm';
import SalespeopleList from './salesFrontEnd/SalespeopleList';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          {/* Inventory */}
          <Route path="/manufacturers" element={<ManufacturersList />} />
          <Route path="/manufacturers/new" element={<ManufacturersForm />} /> 
          <Route path="/models" element={<VehicleModelsList />} />
          <Route path="/models/new" element={<VehicleModelsForm />} />
          <Route path="/automobiles" element={<AutomobilesList />} />
          <Route path="/automobiles/new" element={<AutomobilesForm />} />
          {/* Service */}
          <Route path="/technicians/new" element={<TechniciansForm />} />
          <Route path="/technicians" element={<TechniciansList />} />
          <Route path="/appointments/new" element={<AppointmentsForm />} />
          <Route path="/appointments" element={<AppointmentsList />} />
          <Route path="/appointments/vin" element={<AppointmentsListVin />} />
          <Route path="/appointments/history" element={<AppointmentsHistory/>} />
          {/* Sales */}
          <Route path="/customers" element={<CustomersList />} />
          <Route path="/customers/new" element={<CustomersForm />} />
          <Route path="/sales" element={<SalesList />} />
          <Route path="/sales/new" element={<SalesForm />} />
          <Route path="/salespeople" element={<SalespeopleList />} />
          <Route path="/salespeople/new" element={<SalespeopleForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
