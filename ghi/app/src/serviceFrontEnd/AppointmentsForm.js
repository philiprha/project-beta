import React, {useState, useEffect} from 'react';
import TechniciansList from './TechniciansList.js';

// grab vins from automobiles and makes a vip vin list
function GrabVins(automobiles) {
    let vins = [];
    for (let automobile of automobiles) {
        vins.push(automobile.vin)}
    return vins;
}



function AppointmentsForm (props) {
    const [technicians, setTechnicians] = useState([]);
    const [vipVins, setVipVins] = useState([]);
    const [formData, setFormData] = useState({
        vip: false,
        date: "",
        datetime: "",
        reason: "",
        status: "In Process",
        vin: "",
        customer_name: "",
        technician: "",
    });

    const getTechnicians = async () => {
        const response = await fetch('http://localhost:8080/api/technicians/');
        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians);
            
    }
    }

    // get a list of all the automobiles and extract vin//
    const getVipVins = async () => {
        const response = await fetch('http://localhost:8100/api/automobiles/');
        if (response.ok) {
            const data = await response.json();
            setVipVins(GrabVins(data.autos))
        }
    }
    useEffect(()=>{
        getVipVins()
        getTechnicians()
        }, [])
    
    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    }
    const handleSubmit = async (event) => {
        event.preventDefault();
        

        const url = "http://localhost:8080/api/appointments/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json'
            },
        };
  

        const response = await fetch(url, fetchConfig);
 
        if (response.ok){
            setFormData({
                vip: false,
                date: "",
                datetime: "",
                reason: "",
                status: "In Process",
                vin: "",
                customer_name: "",
                technician: "",
                })
        }
    }

    function setVip() {
        if (vipVins.includes(formData.vin)) {
            setFormData({
                ...formData,
                vip: true
            })
        }
    }
    return(
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Create a new Appointment</h1>
            <form onSubmit={handleSubmit} id="create-technician-form">
                <div className="form-floating mb-3">
                <input value={formData.date} placeholder = "date" onChange={handleFormChange} type="date" name="date" id="date" className="form-control" />
                <label htmlFor="date">Date</label>
                </div> 
                <div className="form-floating mb-3">
                <input value={formData.datetime} placeholder = "time" onChange={handleFormChange} type="time" name="datetime" id="datetime" className="form-control" />
                <label htmlFor="time">Time</label>
                </div>
                <div className="form-floating mb-3">
                <input value={formData.reason} onChange={handleFormChange} placeholder="reason" required type="text" name="reason" id="last_name" className="form-control" />
                <label htmlFor="reason">Reason</label>
                </div> 
                <div className="form-floating mb-3">
                <input value={formData.vin} onChange={handleFormChange} placeholder="vin" required type="text" name="vin" id="vin" className="form-control" />
                <label htmlFor="vin">VIN</label>
                </div> 
                <div className="form-floating mb-3">
                <input value={formData.customer_name} onChange={handleFormChange} placeholder="customer_name" required type="text" name="customer_name" id="customer_name" className="form-control" />
                <label htmlFor="customername">Customer Name</label>
                </div> 
                <div className="form-floating mb-3">
                <select value={formData.technician} onChange={handleFormChange} name= "technician" id="technician" className="form-select py-3">
                        <option value="">Technician ID</option>

                        {technicians.map(technician => { 
                        return (
                                <option key={technician.id} value={technician.employee_id}>
                                {technician.employee_id}
                                </option>
                                );
                        })}
                    </select>
                </div> 
            <button className="btn btn-primary w-100" onClick={setVip}>Create</button>
          </form>
        </div>
        </div>
        <TechniciansList />        
        </div>
    )
}

export default AppointmentsForm;