import React, { useEffect, useState} from 'react';


function AppointmentsHistory(prop) {
    const [appointments, setAppointments] = useState([]);
    
   
    const getAppointments = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/');
        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments)
        
            
    }
    }
    
    // const updatestatus = async (e) => {
        
    //     const value = e.target.value;
    //     const id = e.target.id;
        
    //     const url ="http://localhost:8080/api/appointments/"+id+"/";
    //     const fetchConfig = {
    //         method: "PUT",
    //         body: JSON.stringify({status: value}),
    //         headers: {
    //             'Content-Type': 'application/json'
    //         },
    //     };
    //     const response = await fetch(url, fetchConfig);
    //     getAppointments()
    // }

    useEffect(()=>{
        getAppointments()
        }, [])
    
    return (  
            <div> 
                <div className="row">
                    Appointments History
                </div>



                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Reason</th>
                            <th>VIN</th>
                            <th>Customer Name</th>
                            <th>Technician</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        {appointments.filter(filteredappointment => filteredappointment.status === "Completed" || "Canceled").map(appointment => {
                            return(
                                <tr key={appointment.id}>
                                    <td>{appointment.date}</td>
                                    <td>{appointment.datetime}</td>
                                    <td>{appointment.reason}</td>
                                    <td>{appointment.vin}</td>
                                    <td>{appointment.customer_name}</td>
                                    <td>{appointment.technician.employee_id}</td>
                                    <td>{appointment.status}</td>                                    
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>

        
        
        
        
            )
        
}

export default AppointmentsHistory;