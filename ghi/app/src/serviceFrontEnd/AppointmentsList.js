import React, { useEffect, useState} from 'react';
import TechniciansList from './TechniciansList.js';



function AppointmentsList(prop) {
    const [appointments, setAppointments] = useState([]);
    
   
    const getAppointments = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/');
        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments)
        
            
    }
    }
    
    const updateStatus = async (e) => {
        
        const value = e.target.placeholder;
        const id = e.target.id;
        
        const url ="http://localhost:8080/api/appointments/"+id+"/";
        const fetchConfig = {
            method: "PUT",
            body: JSON.stringify({status: value}),
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            getAppointments()
        }
    }

    useEffect(()=>{
        getAppointments()
        }, [])
    
    return (  
            <div> 
                <div className="row">
                    Only Showing Current Appointments
                </div>



                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>VIP STATUS</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Reason</th>
                            <th>VIN</th>
                            <th>Customer Name</th>
                            <th>Technician</th>
                            <th>Status</th>
                            <th>Status Update</th>
                        </tr>
                    </thead>
                    <tbody>
                        {appointments.filter(filteredappointment => filteredappointment.status === "In Process").map(appointment => {
                            return(
                                <tr key={appointment.id}>
                                    <td>{String(appointment.vip ? 'Yes' : 'No')}</td>
                                    <td>{appointment.date}</td>
                                    <td>{appointment.datetime}</td>
                                    <td>{appointment.reason}</td>
                                    <td>{appointment.vin}</td>
                                    <td>{appointment.customer_name}</td>
                                    <td>{appointment.technician.employee_id}</td>
                                    <td>{appointment.status}</td>
                                    <td>
                                    <input onClick={updateStatus} placeholder= "Canceled" className="styled" name= "status" id={appointment.id} type="button" value ="Cancel"/>
                                    <input onClick={updateStatus} placeholder= "Completed" className="styled" name= "status" id={appointment.id} type="button" value ="Complete"/>
                                    
                                    </td>
                                    
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
                <TechniciansList />
            </div>

        
        
        
        
            )
        
}

export default AppointmentsList;