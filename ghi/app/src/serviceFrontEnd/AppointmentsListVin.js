import React, {useEffect, useState} from 'react';
import TechniciansList from './TechniciansList.js';
// removes appointments with duplicate vin number to be used by filter function
function filterAppointments(appointments) {
    let uniqueAppointments = [];
    let uniqueVins = [];
    for (let appointment of appointments) {
        if(!uniqueVins.includes(appointment.vin)) {
            uniqueVins.push(appointment.vin);
            uniqueAppointments.push(appointment)
        }
    }
    return uniqueAppointments;
}

function AppointmentsListVin(prop) {
    const [appointments, setAppointments] = useState([]);
    const [uniqueAppointments, setUniqueAppointments] = useState([]);
    const [formData, setFormData] = useState({
        vin: ""
    });
   
        const getAppointments = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/');
        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments);
            setUniqueAppointments(filterAppointments(data.appointments))
    }
    }
     const handleFilter = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        
        setFormData({
            ...formData,
            [inputName]: value
        });
        
    }

    
    useEffect(()=>{
        getAppointments()
        }, [])   
        
     
     const cancelStatus = async (e) => {
        
        const value = "Canceled"
        const id = e.target.id;
        
        const url ="http://localhost:8080/api/appointments/"+id+"/";
        const fetchConfig = {
            method: "PUT",
            body: JSON.stringify({status: value}),
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            getAppointments()
            
        }    
    }
    const procesStatus = async (e) => {
        
        const value = "In Process";
        const id = e.target.id;
        
        const url ="http://localhost:8080/api/appointments/"+id+"/";
        const fetchConfig = {
            method: "PUT",
            body: JSON.stringify({status: value}),
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            getAppointments()
            
        }
    }
    const completeStatus = async (e) => {
        
        const value = "Completed"
        const id = e.target.id;
        
        const url ="http://localhost:8080/api/appointments/"+id+"/";
        const fetchConfig = {
            method: "PUT",
            body: JSON.stringify({status: value}),
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            getAppointments()
            
        } 
    }

   
    return (  
        <div className="row">    
            <div className="mb-3">
                <div className="offset-3 col-6">
                    Sort By VIN
                </div>
                
                    <select value={formData.vin} onChange={handleFilter} name= "vin" id="vin" className="form-select py-3">
                        <option value="">Select VIN</option>

                        {uniqueAppointments.map(appointment => { 
                        return (
                                <option key={appointment.id} value={appointment.vin}>
                                {appointment.vin}
                                </option>
                                );
                        })}
                    </select>
                    
                
            </div>
            
               
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Reason</th>
                        <th>VIN</th>
                        <th>Customer Name</th>
                        <th>Technician</th>
                        <th>Status</th>
                        <th>Status Update</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.filter(filteredappointment => filteredappointment.vin === formData.vin).sort((a, b) => a.id - b.id).map(appointment => {
                        return(
                            <tr key={appointment.id}>
                                <td>{appointment.date}</td>
                                <td>{appointment.datetime}</td>
                                <td>{appointment.reason}</td>
                                <td>{appointment.vin}</td>
                                <td>{appointment.customer_name}</td>
                                <td>{appointment.technician.employee_id}</td>
                                <td>{appointment.status}</td>
                                <td>
                                    <input onClick={cancelStatus} className="styled" name= "status" id={appointment.id} type="button" value ="Cancel"/>
                                    <input onClick={completeStatus}className="styled" name= "status" id={appointment.id} type="button" value ="Complete"/>
                                    <input onClick={procesStatus} className="styled" name= "status" id={appointment.id} type="button" value ="In Process"/>
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
            <TechniciansList />
        </div>
        
        
        
            )
        
}

export default AppointmentsListVin;