import React, { useEffect, useState } from 'react';

function AutomobileForm (prop) {
    // Get Vehicles
    const [vehicleModels, setVehicleModels] = useState([]);
    const getVehicleModels = async () => {
        const r = await fetch('http://localhost:8100/api/models/');
        if (r.ok) {
            const data = await r.json();
            setVehicleModels(data.models);
        }
    };

    useEffect(()=> {
        getVehicleModels();
    }, []); 
    // ----


    const [formData, setFormData] = useState({
        color: '',
        year: '',
        vin: '',
        model_id: '',
    });

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value,
        });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        const AutomobileUrl = "http://localhost:8100/api/automobiles/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'applications/json'
            },
        };

        const response = await fetch(AutomobileUrl, fetchConfig);

        if (response.ok) {
            setFormData({
                color: "",
                year: "",
                vin: "",
                model_id: "",
            })
        }
    };

    return(
        <div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <div className="text-center">
                <h1>Add New Automobile</h1>
            </div>
            <form onSubmit={handleSubmit} id="create-automobile-form">
                <div className="form-floating mb-3">
                    <input value={formData.color} onChange={handleFormChange} placeholder="color" required type="text" name="color" id="color" className="form-control" />
                    <label htmlFor="name">Color</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={formData.year} onChange={handleFormChange} placeholder="year" required type="text" name="year" id="year" className="form-control" />
                    <label htmlFor="name">Year</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={formData.vin} onChange={handleFormChange} placeholder="vin" required type="text" name="vin" id="vin" className="form-control" />
                    <label htmlFor="name">VIN</label>
                </div>
                <div className="mb-3">
                    <select value={formData.model_id} onChange={handleFormChange} required name="model_id" id="model" className="form-select py-3">
                        <option value="">Choose a Model</option>
                        {vehicleModels.map(model => {
                        return (
                            <option key={model.id} value={model.id}>
                            {model.name}
                            </option>
                            );
                            })}
                    </select>
                </div>    
            <button className="btn btn-primary w-100">Create</button>
            </form>
        </div>
        </div>
        </div>

    )
}

export default AutomobileForm;