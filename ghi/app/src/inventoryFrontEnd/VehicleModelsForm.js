import React, {useState, useEffect} from 'react';

function VehicleModelsForm (props) {
    const [formData, setFormData] = useState({
        name: "",
        picture_url: "",
        manufacturer_id: "",
        
    });
    const [manufacturers, setManufacturers] = useState([])

    const getManufacturers = async () => {
        const response = await fetch('http://localhost:8100/api/manufacturers/');
        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers)
            
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        
        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    useEffect(()=>{
        getManufacturers()
        }, [])

    const handleSubmit = async (event) => {
        event.preventDefault();
    
        const VehicleModelsUrl = "http://localhost:8100/api/models/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json'
            },
        };        
        
        const response = await fetch(VehicleModelsUrl, fetchConfig);
        
        if (response.ok){
            setFormData({
                name: "",
                picture_url: "",
                manufacturer_id:"",
               
            })
        }
    }
    return(
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Create a new Vehicle Model</h1>
            <form onSubmit={handleSubmit} id="create-vehiclemodel-form">
                <div className="form-floating mb-3">
                    <input value={formData.name} onChange={handleFormChange} placeholder="name" required type="text" name="name" id="name" className="form-control" />
                    <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={formData.picture_url} onChange={handleFormChange} placeholder="picture_url" required type="text" name="picture_url" id="picture_url" className="form-control" />
                    <label htmlFor="name">Picture URL</label>
                </div>
                <div className="mb-3">
                    <select value={formData.manufacturer_id} onChange={handleFormChange} required name="manufacturer_id" id="manufacturer" className="form-select py-3">
                    <option value="">Choose a Manufacturer</option>
                    {manufacturers.map(manufacturer => {
                    return (
                        <option key={manufacturer.id} value={manufacturer.id}>
                        {manufacturer.name}
                        </option>
                        );
                        })}
                    </select>
                </div>
            <button className="btn btn-primary w-100">Create</button>
            </form>
            </div>
        </div>
    </div>

    )


}










export default VehicleModelsForm;