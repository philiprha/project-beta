import React, {useEffect, useState} from 'react' ;

function VehicleModelsList(prop) {
    const [vehicleModels, setVehicleModels] = useState([]);  

    const getVehicleModels = async () => {
        const response = await fetch('http://localhost:8100/api/models/');
        
        if (response.ok) {
            const data = await response.json();
            setVehicleModels(data.models)
        }
        
    }
   

    useEffect(()=>{
        getVehicleModels()
    }, [])

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Picture</th>
                    <th>Manufacturer</th>
                </tr>
            </thead>
            <tbody>                              
                {vehicleModels.map(model => {
                        return(
                        <tr key={model.id}>
                            <td>{model.name}</td>
                            <td>
                                <picture>
                                    <source srcset={model.picture_url} alt=""/>
                                    <img src={model.picture_url} alt="" height="200" width="300"/>
                                </picture>
                            </td>
                            <td>{model.manufacturer.name}</td>
                        </tr>
                        )
                })}
            </tbody>
        </table>
    )
}

export default VehicleModelsList;